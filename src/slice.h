#ifndef __kc_SLICE_H__
#define __kc_SLICE_H__

#include "core.h"
#include <stdio.h>

#define KC_SLICE_FUNC_ATTACH(name) kc_##name##_attach
#define KC_SLICE_FUNC_DETACH(name) kc_##name##_detach
#define KC_SLICE_FUNC_FROM(name) kc_##name##_from
#define KC_SLICE_FUNC_MALLOC(name) kc_##name##_malloc
#define KC_SLICE_FUNC_SUB(name) kc_##name##_sub
#define KC_SLICE_FUNC_COPY(name) kc_##name##_copy
#define KC_SLICE_FUNC_EQUAL(name) kc_##name##_equal
#define KC_SLICE_FUNC_FILL_VALUE(name) kc_##name##_fill_value
#define KC_SLICE_FUNC_FILL(name) kc_##name##_fill
#define KC_SLICE_FUNC_STARTS_WITH(name) kc_##name##_starts_with
#define KC_SLICE_FUNC_ENDS_WITH(name) kc_##name##_ends_with
#define KC_SLICE_FUNC_FIRST(name) kc_##name##_first
#define KC_SLICE_FUNC_LAST(name) kc_##name##_last
#define KC_SLICE_FUNC_APPEND(name) kc_##name##_append
#define KC_SLICE_FUNC_COUNT(name) kc_##name##_count
#define KC_SLICE_FUNC_SPLIT(name) kc_##name##_split
#define KC_SLICE_FUNC_STDOUT(name) kc_##name##_stdout

#define KC_TYPE_SLICE_OFFSET(self, type) ((((size_t)(self)->data) - ((size_t)(self->array->pointer))) / (size_t)sizeof(type));

#define KC_TYPE_SLICE_ST(name, type) \
    typedef struct                   \
    {                                \
        kc_array_t *array;           \
        type *data;                  \
        size_t length;               \
    } KC_TYPE_NAME(name);

#define KC_TYPE_SLICE_DECLARE(name, type)                                                                                               \
    KC_TYPE_SLICE_ST(name, type)                                                                                                        \
    void KC_SLICE_FUNC_ATTACH(name)(KC_TYPE_NAME(name) * self, KC_TYPE_NAME(name) * dst);                                               \
    void KC_SLICE_FUNC_DETACH(name)(KC_TYPE_NAME(name) * self);                                                                         \
    BOOL KC_SLICE_FUNC_MALLOC(name)(KC_TYPE_NAME(name) * self, size_t length, size_t capacity);                                         \
    BOOL KC_SLICE_FUNC_FROM(name)(KC_TYPE_NAME(name) * self, const type *src, size_t length);                                           \
    KC_TYPE_NAME(name)                                                                                                                  \
    KC_SLICE_FUNC_SUB(name)                                                                                                             \
    (KC_TYPE_NAME(name) * self, size_t begin, size_t end);                                                                              \
    size_t KC_SLICE_FUNC_COPY(name)(KC_TYPE_NAME(name) * self, const type *src, size_t length);                                         \
    BOOL KC_SLICE_FUNC_EQUAL(name)(KC_TYPE_NAME(name) * self, const type *other, size_t length);                                        \
    void KC_SLICE_FUNC_FILL_VALUE(name)(KC_TYPE_NAME(name) * self, type value);                                                         \
    void KC_SLICE_FUNC_FILL(name)(KC_TYPE_NAME(name) * self);                                                                           \
    BOOL KC_SLICE_FUNC_STARTS_WITH(name)(KC_TYPE_NAME(name) * self, const type *tag, size_t length);                                    \
    BOOL KC_SLICE_FUNC_ENDS_WITH(name)(KC_TYPE_NAME(name) * self, const type *tag, size_t length);                                      \
    size_t KC_SLICE_FUNC_FIRST(name)(KC_TYPE_NAME(name) * self, size_t skip, const type *tag, size_t length);                           \
    size_t KC_SLICE_FUNC_LAST(name)(KC_TYPE_NAME(name) * self, size_t skip, const type *tag, size_t length);                            \
    BOOL KC_SLICE_FUNC_APPEND(name)(KC_TYPE_NAME(name) * self, const type *values, size_t length);                                      \
    size_t KC_SLICE_FUNC_COUNT(name)(KC_TYPE_NAME(name) * self, const type *sub, size_t length);                                        \
    size_t KC_SLICE_FUNC_SPLIT(name)(KC_TYPE_NAME(name) * self, KC_TYPE_NAME(name) * output, size_t n, const type *sep, size_t length); \
    void KC_SLICE_FUNC_STDOUT(name)(KC_TYPE_NAME(name) * self, const char *prefix);

#define KC_TYPE_SLICE_IMPLEMENT_ATTACH(name, type)                                       \
    void KC_SLICE_FUNC_ATTACH(name)(KC_TYPE_NAME(name) * self, KC_TYPE_NAME(name) * dst) \
    {                                                                                    \
        KC_SLICE_FUNC_DETACH(name)                                                       \
        (dst);                                                                           \
        dst->data = self->data;                                                          \
        dst->length = self->length;                                                      \
        dst->array = self->array;                                                        \
        if (dst->array)                                                                  \
        {                                                                                \
            kc_array_increment(dst->array);                                              \
        }                                                                                \
    }

#define KC_TYPE_SLICE_IMPLEMENT_DETACH(name, type)             \
    void KC_SLICE_FUNC_DETACH(name)(KC_TYPE_NAME(name) * self) \
    {                                                          \
        if (kc_array_decrement(self->array) == 1)              \
        {                                                      \
            kc_free(self->array);                              \
        }                                                      \
        self->array = 0;                                       \
        self->data = 0;                                        \
        self->length = 0;                                      \
    }

#define KC_TYPE_SLICE_IMPLEMENT_FROM(name, type)                                             \
    BOOL KC_SLICE_FUNC_FROM(name)(KC_TYPE_NAME(name) * self, const type *src, size_t length) \
    {                                                                                        \
        if (!length)                                                                         \
        {                                                                                    \
            KC_SLICE_FUNC_DETACH(name)                                                       \
            (self);                                                                          \
            return TRUE;                                                                     \
        }                                                                                    \
        size_t size = sizeof(type) * length;                                                 \
        void *p = kc_malloc(size);                                                           \
        if (!p)                                                                              \
        {                                                                                    \
            return FALSE;                                                                    \
        }                                                                                    \
        kc_array_t *arr = (kc_array_t *)kc_malloc(sizeof(kc_array_t));                       \
        if (!arr)                                                                            \
        {                                                                                    \
            kc_free(p);                                                                      \
            return FALSE;                                                                    \
        }                                                                                    \
        KC_SLICE_FUNC_DETACH(name)                                                           \
        (self);                                                                              \
        memcpy(p, src, size);                                                                \
        arr->pointer = p;                                                                    \
        arr->length = length;                                                                \
        arr->reference = 1;                                                                  \
        self->array = arr;                                                                   \
        self->data = (type *)p;                                                              \
        self->length = length;                                                               \
        return TRUE;                                                                         \
    }
#define KC_TYPE_SLICE_IMPLEMENT_MALLOC(name, type)                                             \
    BOOL KC_SLICE_FUNC_MALLOC(name)(KC_TYPE_NAME(name) * self, size_t length, size_t capacity) \
    {                                                                                          \
        if (capacity < length)                                                                 \
        {                                                                                      \
            capacity = length;                                                                 \
        }                                                                                      \
        if (!capacity)                                                                         \
        {                                                                                      \
            KC_SLICE_FUNC_DETACH(name)                                                         \
            (self);                                                                            \
            return TRUE;                                                                       \
        }                                                                                      \
        size_t size = sizeof(type) * capacity;                                                 \
        void *p = kc_malloc(size);                                                             \
        if (!p)                                                                                \
        {                                                                                      \
            return FALSE;                                                                      \
        }                                                                                      \
        kc_array_t *arr = (kc_array_t *)kc_malloc(sizeof(kc_array_t));                         \
        if (!arr)                                                                              \
        {                                                                                      \
            kc_free(p);                                                                        \
            return FALSE;                                                                      \
        }                                                                                      \
        KC_SLICE_FUNC_DETACH(name)                                                             \
        (self);                                                                                \
        memset(p, 0, size);                                                                    \
        arr->pointer = p;                                                                      \
        arr->length = capacity;                                                                \
        arr->reference = 1;                                                                    \
        self->array = arr;                                                                     \
        self->data = (type *)p;                                                                \
        self->length = length;                                                                 \
        return TRUE;                                                                           \
    }
#define KC_TYPE_SLICE_IMPLEMENT_SUB(name, type)           \
    KC_TYPE_NAME(name)                                    \
    KC_SLICE_FUNC_SUB(name)                               \
    (KC_TYPE_NAME(name) * self, size_t begin, size_t end) \
    {                                                     \
        KC_VAR(KC_TYPE_NAME(name), result);               \
        size_t offset = 0;                                \
        if (self->length)                                 \
        {                                                 \
            if (begin < 0)                                \
            {                                             \
                begin = 0;                                \
            }                                             \
            else if (begin > self->length)                \
            {                                             \
                begin = self->length;                     \
            }                                             \
            offset = begin;                               \
            if (end > begin)                              \
            {                                             \
                result.length = end - begin;              \
                size_t max = self->length - offset;       \
                if (result.length > max)                  \
                {                                         \
                    result.length = max;                  \
                }                                         \
            }                                             \
        }                                                 \
        result.array = self->array;                       \
        result.data = self->data + offset;                \
        if (self->array)                                  \
        {                                                 \
            kc_array_increment(self->array);              \
        }                                                 \
        return result;                                    \
    }

#define KC_TYPE_SLICE_IMPLEMENT_COPY(name, type)                                               \
    size_t KC_SLICE_FUNC_COPY(name)(KC_TYPE_NAME(name) * self, const type *src, size_t length) \
    {                                                                                          \
        if (length > self->length)                                                             \
        {                                                                                      \
            length = self->length;                                                             \
        }                                                                                      \
        if (length)                                                                            \
        {                                                                                      \
            size_t size = sizeof(type) * length;                                               \
            memcpy(self->data, src, size);                                                     \
        }                                                                                      \
        return length;                                                                         \
    }

#define KC_TYPE_SLICE_IMPLEMENT_EQUAL(name, type)                                               \
    BOOL KC_SLICE_FUNC_EQUAL(name)(KC_TYPE_NAME(name) * self, const type *other, size_t length) \
    {                                                                                           \
        if (!length)                                                                            \
        {                                                                                       \
            return (!self || !self->length) ? TRUE : FALSE;                                     \
        }                                                                                       \
        if (self->length != length)                                                             \
        {                                                                                       \
            return FALSE;                                                                       \
        }                                                                                       \
        size_t size = sizeof(type) * length;                                                    \
        return !memcmp(self->data, other, size) ? TRUE : FALSE;                                 \
    }

#define KC_TYPE_SLICE_IMPLEMENT_FILL(name, type)                               \
    void KC_SLICE_FUNC_FILL_VALUE(name)(KC_TYPE_NAME(name) * self, type value) \
    {                                                                          \
        if (self->length)                                                      \
        {                                                                      \
            for (size_t i = 0; i < self->length; ++i)                          \
            {                                                                  \
                self->data[i] = value;                                         \
            }                                                                  \
        }                                                                      \
    }

#define KC_TYPE_SLICE_IMPLEMENT_FILL_0(name, type)           \
    void KC_SLICE_FUNC_FILL(name)(KC_TYPE_NAME(name) * self) \
    {                                                        \
        if (self->length)                                    \
        {                                                    \
            size_t size = sizeof(type) * self->length;       \
            memset(self->data, 0, size);                     \
        }                                                    \
    }

#define KC_TYPE_SLICE_IMPLEMENT_STARTS_WITH(name, type)                                             \
    BOOL KC_SLICE_FUNC_STARTS_WITH(name)(KC_TYPE_NAME(name) * self, const type *tag, size_t length) \
    {                                                                                               \
        if (!length)                                                                                \
        {                                                                                           \
            return TRUE;                                                                            \
        }                                                                                           \
        if (self->length < length)                                                                  \
        {                                                                                           \
            return FALSE;                                                                           \
        }                                                                                           \
        size_t size = sizeof(type) * length;                                                        \
        return !memcmp(self->data, tag, size) ? TRUE : FALSE;                                       \
    }

#define KC_TYPE_SLICE_IMPLEMENT_ENDS_WITH(name, type)                                             \
    BOOL KC_SLICE_FUNC_ENDS_WITH(name)(KC_TYPE_NAME(name) * self, const type *tag, size_t length) \
    {                                                                                             \
        if (!length)                                                                              \
        {                                                                                         \
            return TRUE;                                                                          \
        }                                                                                         \
        if (self->length < length)                                                                \
        {                                                                                         \
            return FALSE;                                                                         \
        }                                                                                         \
        size_t size = sizeof(type) * length;                                                      \
        return !memcmp(self->data + self->length - length, tag, size) ? TRUE : FALSE;             \
    }

#define KC_TYPE_SLICE_IMPLEMENT_FIRST(name, type)                                                            \
    size_t KC_SLICE_FUNC_FIRST(name)(KC_TYPE_NAME(name) * self, size_t skip, const type *tag, size_t length) \
    {                                                                                                        \
        size_t result = KC_INVALID_SIZE_T;                                                                   \
        if (self)                                                                                            \
        {                                                                                                    \
            if (!length)                                                                                     \
            {                                                                                                \
                return 0;                                                                                    \
            }                                                                                                \
            if (skip + length > self->length)                                                                \
            {                                                                                                \
                return result;                                                                               \
            }                                                                                                \
            size_t end = self->length - length + 1;                                                          \
            size_t size = sizeof(type) * length;                                                             \
            for (size_t i = skip; i < end; ++i)                                                              \
            {                                                                                                \
                if (!memcmp(self->data + i, tag, size))                                                      \
                {                                                                                            \
                    result = i;                                                                              \
                    break;                                                                                   \
                }                                                                                            \
            }                                                                                                \
        }                                                                                                    \
        return result;                                                                                       \
    }

#define KC_TYPE_SLICE_IMPLEMENT_LAST(name, type)                                                            \
    size_t KC_SLICE_FUNC_LAST(name)(KC_TYPE_NAME(name) * self, size_t skip, const type *tag, size_t length) \
    {                                                                                                       \
        size_t result = KC_INVALID_SIZE_T;                                                                  \
        if (self)                                                                                           \
        {                                                                                                   \
            if (!length)                                                                                    \
            {                                                                                               \
                return 0;                                                                                   \
            }                                                                                               \
            if (skip + length > self->length)                                                               \
            {                                                                                               \
                return result;                                                                              \
            }                                                                                               \
            size_t end = self->length - length + 1;                                                         \
            size_t size = sizeof(type) * length;                                                            \
            for (size_t i = end - skip; i > 0; --i)                                                         \
            {                                                                                               \
                if (!memcmp(self->data + i - 1, tag, size))                                                 \
                {                                                                                           \
                    result = i - 1;                                                                         \
                    break;                                                                                  \
                }                                                                                           \
            }                                                                                               \
        }                                                                                                   \
        return result;                                                                                      \
    }

#define KC_TYPE_SLICE_IMPLEMENT_APPEND(name, type)                                                \
    BOOL KC_SLICE_FUNC_APPEND(name)(KC_TYPE_NAME(name) * self, const type *values, size_t length) \
    {                                                                                             \
        if (!length)                                                                              \
        {                                                                                         \
            return TRUE;                                                                          \
        }                                                                                         \
        size_t min = self->length + length;                                                       \
        size_t capacity;                                                                          \
        if (self->array)                                                                          \
        {                                                                                         \
            size_t offset = KC_TYPE_SLICE_OFFSET(self, type);                                     \
            capacity = self->array->length - offset;                                              \
            if (min <= capacity)                                                                  \
            {                                                                                     \
                size_t size = sizeof(type) * length;                                              \
                memcpy(self->data + self->length, values, size);                                  \
                self->length += length;                                                           \
                return TRUE;                                                                      \
            }                                                                                     \
            if (!capacity)                                                                        \
            {                                                                                     \
                capacity = length;                                                                \
            }                                                                                     \
        }                                                                                         \
        else                                                                                      \
        {                                                                                         \
            capacity = length;                                                                    \
        }                                                                                         \
        size_t count = 60;                                                                        \
        while (capacity < min)                                                                    \
        {                                                                                         \
            capacity *= 2;                                                                        \
            if (!(--count))                                                                       \
            {                                                                                     \
                return FALSE;                                                                     \
            }                                                                                     \
        }                                                                                         \
        KC_VAR(KC_TYPE_NAME(name), tmp);                                                          \
        if (KC_SLICE_FUNC_MALLOC(name)(&tmp, min, capacity))                                      \
        {                                                                                         \
            size_t size = sizeof(type);                                                           \
            if (self->length)                                                                     \
            {                                                                                     \
                memcpy(tmp.data, self->data, size * self->length);                                \
            }                                                                                     \
            memcpy(tmp.data + self->length, values, size * length);                               \
            KC_SLICE_FUNC_DETACH(name)                                                            \
            (self);                                                                               \
            *self = tmp;                                                                          \
            return TRUE;                                                                          \
        }                                                                                         \
        return FALSE;                                                                             \
    }

#define KC_TYPE_SLICE_IMPLEMENT_COUNT(name, type)                                               \
    size_t KC_SLICE_FUNC_COUNT(name)(KC_TYPE_NAME(name) * self, const type *sub, size_t length) \
    {                                                                                           \
        size_t result = 0;                                                                      \
        if (!length)                                                                            \
        {                                                                                       \
            return result;                                                                      \
        }                                                                                       \
        size_t skip = 0;                                                                        \
        while (1)                                                                               \
        {                                                                                       \
            skip = KC_SLICE_FUNC_FIRST(name)(self, skip, sub, length);                          \
            if (KC_INVALID_SIZE_T == skip)                                                      \
            {                                                                                   \
                break;                                                                          \
            }                                                                                   \
            ++result;                                                                           \
            skip += length;                                                                     \
        }                                                                                       \
        return result;                                                                          \
    }

#define KC_TYPE_SLICE_IMPLEMENT_SPLIT(name, type)                                                                                      \
    size_t KC_SLICE_FUNC_SPLIT(name)(KC_TYPE_NAME(name) * self, KC_TYPE_NAME(name) * output, size_t n, const type *sep, size_t length) \
    {                                                                                                                                  \
        if (!n)                                                                                                                        \
        {                                                                                                                              \
            return 0;                                                                                                                  \
        }                                                                                                                              \
        if (!length || 1 == n)                                                                                                         \
        {                                                                                                                              \
            KC_SLICE_FUNC_ATTACH(name)                                                                                                 \
            (self, output);                                                                                                            \
            return 1;                                                                                                                  \
        }                                                                                                                              \
        size_t result = 0;                                                                                                             \
        size_t skip = 0;                                                                                                               \
        size_t offset = 0;                                                                                                             \
        size_t i = 0;                                                                                                                  \
        const size_t max = n - 1;                                                                                                      \
        KC_TYPE_NAME(name)                                                                                                             \
        tmp;                                                                                                                           \
        while (1)                                                                                                                      \
        {                                                                                                                              \
            ++result;                                                                                                                  \
            if (max == i)                                                                                                              \
            {                                                                                                                          \
                tmp = KC_SLICE_FUNC_SUB(name)(self, skip, self->length);                                                               \
                KC_SLICE_FUNC_DETACH(name)                                                                                             \
                (output + i);                                                                                                          \
                output[i] = tmp;                                                                                                       \
                break;                                                                                                                 \
            }                                                                                                                          \
            offset = KC_SLICE_FUNC_FIRST(name)(self, skip, sep, length);                                                               \
            if (KC_INVALID_SIZE_T == offset)                                                                                           \
            {                                                                                                                          \
                tmp = KC_SLICE_FUNC_SUB(name)(self, skip, self->length);                                                               \
                KC_SLICE_FUNC_DETACH(name)                                                                                             \
                (output + i);                                                                                                          \
                output[i] = tmp;                                                                                                       \
                break;                                                                                                                 \
            }                                                                                                                          \
            tmp = KC_SLICE_FUNC_SUB(name)(self, skip, offset);                                                                         \
            KC_SLICE_FUNC_DETACH(name)                                                                                                 \
            (output + i);                                                                                                              \
            output[i++] = tmp;                                                                                                         \
            skip = offset + length;                                                                                                    \
        }                                                                                                                              \
        return result;                                                                                                                 \
    }

#define KC_TYPE_SLICE_IMPLEMENT_STDOUT(name, type)                                           \
    void KC_SLICE_FUNC_STDOUT(name)(KC_TYPE_NAME(name) * self, const char *prefix)           \
    {                                                                                        \
        const char *tag = prefix ? prefix : "";                                              \
        if (self)                                                                            \
        {                                                                                    \
            if (self->array)                                                                 \
            {                                                                                \
                size_t offset = KC_TYPE_SLICE_OFFSET(self, type);                            \
                printf("%s%s {\n%s   array = %p\n", tag, KC_OUTPUT_STR2(KC_TYPE_NAME(name)), \
                       tag, self->array);                                                    \
                kc_array_stdout(self->array, "   ");                                         \
                printf("%s   array = %p\n%s   offset = %ld\n%s   length = %ld\n%s}\n",       \
                       tag, self->data,                                                      \
                       tag, offset,                                                          \
                       tag, self->length,                                                    \
                       tag);                                                                 \
            }                                                                                \
            else                                                                             \
            {                                                                                \
                printf("%s%s {\n%s   array = %p\n%s   data = %p\n%s   length = %ld\n%s}\n",  \
                       tag, KC_OUTPUT_STR2(KC_TYPE_NAME(name)),                              \
                       tag, self->array,                                                     \
                       tag, self->data,                                                      \
                       tag, self->length,                                                    \
                       tag);                                                                 \
            }                                                                                \
        }                                                                                    \
        else                                                                                 \
        {                                                                                    \
            printf("%s%s (nil)\n", tag, KC_OUTPUT_STR2(KC_TYPE_NAME(name)));                 \
        }                                                                                    \
    }

#define KC_TYPE_SLICE_IMPLEMENT(name, type)         \
    KC_TYPE_SLICE_IMPLEMENT_ATTACH(name, type)      \
    KC_TYPE_SLICE_IMPLEMENT_DETACH(name, type)      \
    KC_TYPE_SLICE_IMPLEMENT_MALLOC(name, type)      \
    KC_TYPE_SLICE_IMPLEMENT_FROM(name, type)        \
    KC_TYPE_SLICE_IMPLEMENT_SUB(name, type)         \
    KC_TYPE_SLICE_IMPLEMENT_COPY(name, type)        \
    KC_TYPE_SLICE_IMPLEMENT_EQUAL(name, type)       \
    KC_TYPE_SLICE_IMPLEMENT_FILL(name, type)        \
    KC_TYPE_SLICE_IMPLEMENT_FILL_0(name, type)      \
    KC_TYPE_SLICE_IMPLEMENT_STARTS_WITH(name, type) \
    KC_TYPE_SLICE_IMPLEMENT_ENDS_WITH(name, type)   \
    KC_TYPE_SLICE_IMPLEMENT_FIRST(name, type)       \
    KC_TYPE_SLICE_IMPLEMENT_LAST(name, type)        \
    KC_TYPE_SLICE_IMPLEMENT_APPEND(name, type)      \
    KC_TYPE_SLICE_IMPLEMENT_COUNT(name, type)       \
    KC_TYPE_SLICE_IMPLEMENT_SPLIT(name, type)       \
    KC_TYPE_SLICE_IMPLEMENT_STDOUT(name, type)

#if defined(__cplusplus)
extern "C"
{
#endif

#define KC_SLICE_BOXING(self) ((self)->data), ((self)->length)

#ifndef KC_STRING_DISABLED
    KC_TYPE_SLICE_DECLARE(string, char)
    void kc_string_stdout_ex(kc_string_t *self, const char *prefix);

#ifndef KC_C_STRING_BOXING
#define KC_C_STRING_BOXING(str) (str), strlen((str))
#endif // KC_S_STRING

#endif

#if defined(__cplusplus)
}
#endif /* end 'extern "C"' wrapper */
#endif // __kc_SLICE_H__