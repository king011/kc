#ifndef __kc_CORE_H__
#define __kc_CORE_H__

#include <stdlib.h>
#include <string.h>
#ifndef __cplusplus
#include <stdatomic.h>
#else
#include <atomic>
typedef std::atomic_size_t atomic_size_t;
#endif

#if defined(__cplusplus)
extern "C"
{
#endif

#ifndef BOOL
#define BOOL unsigned char
#endif //BOOL
#ifndef TRUE
#define TRUE 1
#endif //BOOL
#ifndef FALSE
#define FALSE 0
#endif //BOOL

#define KC_OUTPUT_STR1(R) #R
#define KC_OUTPUT_STR2(R) KC_OUTPUT_STR1(R)

#define KC_VAR(type, var) \
    type var;             \
    memset(&var, 0, sizeof(type));

#define KC_NAME(name) kc_##name
#define KC_TYPE_NAME(name) kc_##name##_t

#define KC_INVALID_SIZE_T ((size_t)-1)

    typedef void *(*kc_malloc_ft)(size_t);
    typedef void (*kc_free_ft)(void *);
    /**
    *	\brief 設置內存分配器 所有 kc 模塊將 使用 此分配器 進行內存分配
    */
    void kc_set_allocator(kc_malloc_ft m, kc_free_ft f);
    /**
    *	\brief 分配內存
    */
    void *kc_malloc(size_t n);
    /**
    *	\brief 釋放內存
    */
    void kc_free(void *p);
    /**
    *	\brief 一個帶引用計數的指針
    */
    typedef struct
    {
        /**
        *	\brief 指針地址
        */
        void *pointer;
        /**
        *	\brief 引用計數
        */
        atomic_size_t reference;
    } kc_pointer_t;

    /**
    *	\brief 使引用計數自增
    *
    *   \note 如果 pointer 沒有設置 則 不進行任何操作
    */
    size_t kc_pointer_increment(kc_pointer_t *self);
    /**
    *	\brief 使引用計數自減
    *
    *   \note 如果 pointer 沒有設置 則 不進行任何操作 當計數減少到0將 自動釋放內存
    */
    size_t kc_pointer_decrement(kc_pointer_t *self);
    /**
     * \brief 動態申請 size 大小的 內存
     * 
     * 如果 pointer 已經保存了一個 原始指針 會先自動調用 kc_free
     */
    BOOL kc_pointer_init(kc_pointer_t *self, size_t size);

    /**
     * \brief 將一個原始指針 設置給 kc_pointer_t
     * 
     * 如果 pointer 已經保存了一個 原始指針 會先自動調用 kc_free
     */
    BOOL kc_pointer_set(kc_pointer_t *self, void *pointer);
    /**
     *  \brief 輸出測試信息到 stdout
     */
    void kc_pointer_stdout(kc_pointer_t *self, const char *prefix);

    /**
    *	\brief 一個帶引用計數的數組指針
    */
    typedef struct
    {
        /**
        *	\brief 數組地址
        */
        void *pointer;
        /**
        *	\brief 數組大小
        */
        size_t length;
        /**
        *	\brief 引用計數
        */
        atomic_size_t reference;
    } kc_array_t;

    /**
    *	\brief 使引用計數自增
    *
    *   \note 如果 pointer 沒有設置 則 不進行任何操作
    */
    size_t kc_array_increment(kc_array_t *self);
    /**
    *	\brief 使引用計數自減
    *
    *   \note 如果 pointer 沒有設置 則 不進行任何操作 當計數減少到0將 自動釋放內存
    */
    size_t kc_array_decrement(kc_array_t *self);
    /**
     * \brief 動態申請 size 大小的 內存
     * 
     * 如果 pointer 已經保存了一個 原始指針 會先自動調用 kc_free
     */
    BOOL kc_array_init(kc_array_t *self, const size_t size, const size_t length);
    /**
     * \brief 設置原始指針
     *
     * 如果 pointer 已經保存了一個 原始指針 會先自動調用 kc_free
     */
    BOOL kc_array_set(kc_array_t *self, void *pointer, const size_t length);
    /**
     *  \brief 輸出測試信息到 stdout
     */
    void kc_array_stdout(kc_array_t *self, const char *prefix);

#if defined(__cplusplus)
}
#endif /* end 'extern "C"' wrapper */

#endif // __kc_CORE_H__