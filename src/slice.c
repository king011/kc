#include "slice.h"

#ifndef KC_STRING_DISABLED
KC_TYPE_SLICE_IMPLEMENT(string, char)
void kc_string_stdout_ex(kc_string_t *self, const char *prefix)
{
    const char *tag = prefix ? prefix : "";
    if (self)
    {
        if (self->array)
        {
            size_t offset = KC_TYPE_SLICE_OFFSET(self, char);
            printf("%skc_string_t {\n%s   array = %p\n", tag,
                   tag, self->array);
            kc_array_stdout(self->array, "   ");
            printf("%s   array = %p\n%s   offset = %ld\n%s   length = %ld\n%s   str = [",
                   tag, self->data,
                   tag, offset,
                   tag, self->length,
                   tag);
            for (size_t i = 0; i < self->length; i++)
            {
                putchar(self->data[i]);
            }
            printf("]\n%s}\n", tag);
        }
        else
        {
            printf("%skc_string_t {\n%s   array = %p\n%s   data = %p\n%s   length = %ld\n%s}\n",
                   tag,
                   tag, self->array,
                   tag, self->data,
                   tag, self->length,
                   tag);
        }
    }
    else
    {
        printf("%skc_string_t (nil)\n", tag);
    }
}
#endif
