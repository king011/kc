#include "core.h"
#include <stdio.h>
kc_malloc_ft g_kc_malloc = 0;
kc_free_ft g_kc_free = 0;

void kc_set_allocator(kc_malloc_ft m, kc_free_ft f)
{
    g_kc_malloc = m;
    g_kc_free = f;
}
void *kc_malloc(size_t n)
{
    return g_kc_malloc ? g_kc_malloc(n) : malloc(n);
}
void kc_free(void *self)
{
    return g_kc_free ? g_kc_free(self) : free(self);
}
size_t kc_pointer_increment(kc_pointer_t *self)
{
    if (!self || !self->pointer)
    {
        return FALSE;
    }
    return atomic_fetch_add(&self->reference, 1);
}
size_t kc_pointer_decrement(kc_pointer_t *self)
{
    if (!self || !self->pointer)
    {
        return FALSE;
    }

    size_t old = atomic_fetch_sub(&self->reference, 1);
    if (old == 1)
    {
        kc_free((self)->pointer);
        self->pointer = 0;
    }
    return old;
}
BOOL kc_pointer_set(kc_pointer_t *self, void *pointer)
{
    if (!self || !pointer || self->pointer == pointer)
    {
        return FALSE;
    }
    if (self->pointer)
    {
        kc_free(self->pointer);
    }
    self->pointer = pointer;
    self->reference = pointer ? 1 : 0;
    return TRUE;
}
BOOL kc_pointer_init(kc_pointer_t *self, size_t size)
{
    if (!self)
    {
        return FALSE;
    }

    void *ptr = kc_malloc(size);
    if (!ptr)
    {
        return FALSE;
    }
    if (self->pointer)
    {
        kc_free(self->pointer);
    }
    self->pointer = ptr;
    self->reference = 1;
    return TRUE;
}
void kc_pointer_stdout(kc_pointer_t *self, const char *prefix)
{
    const char *tag = prefix ? prefix : "";
    if (self)
    {
        printf("%skc_pointer_t = {\n%s   pointer = %p\n%s   reference = %ld\n%s}\n",
               tag,
               tag, self->pointer,
               tag, self->reference,
               tag);
    }
    else
    {
        printf("%skc_pointer_t (nil)\n", tag);
    }
}

size_t kc_array_increment(kc_array_t *self)
{
    if (!self || !self->pointer)
    {
        return 0;
    }
    return atomic_fetch_add(&self->reference, 1);
}
size_t kc_array_decrement(kc_array_t *self)
{
    if (!self || !self->pointer)
    {
        return 0;
    }

    size_t old = atomic_fetch_sub(&self->reference, 1);
    if (old == 1)
    {
        kc_free((self)->pointer);
        self->pointer = 0;
        self->length = 0;
    }
    return old;
}
BOOL kc_array_set(kc_array_t *self, void *pointer, const size_t length)
{
    if (!self || !pointer || self->pointer == pointer)
    {
        return FALSE;
    }
    if (self->pointer)
    {
        kc_free(self->pointer);
    }
    self->pointer = pointer;
    self->length = length;
    self->reference = length ? 1 : 0;
    return TRUE;
}
BOOL kc_array_init(kc_array_t *self, const size_t size, const size_t length)
{
    if (!self)
    {
        return FALSE;
    }

    void *ptr = kc_malloc(size * length);
    if (!ptr)
    {
        return FALSE;
    }
    if (self->pointer)
    {
        kc_free(self->pointer);
    }
    self->pointer = ptr;
    self->length = length;
    self->reference = 1;
    return TRUE;
}
void kc_array_stdout(kc_array_t *self, const char *prefix)
{
    const char *tag = prefix ? prefix : "";
    if (self)
    {
        printf("%skc_array_t = {\n%s   pointer = %p\n%s   length = %ld\n%s   reference = %ld\n%s}\n",
               tag,
               tag, self->pointer,
               tag, self->length,
               tag, self->reference,
               tag);
    }
    else
    {
        printf("%skc_array_t (nil)\n", tag);
    }
}
