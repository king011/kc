# Goolge test framework
find_package(GTest REQUIRED)
list(APPEND target_headers ${GTEST_INCLUDE_DIRS})
list(APPEND target_libs ${GTEST_LIBRARIES})

# 刪除 重複項
if(target_headers)
    list(REMOVE_DUPLICATES target_headers)
endif()
if(target_libs)
    list(REMOVE_DUPLICATES target_libs)
endif()
