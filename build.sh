#!/bin/bash
#Program:
#       c++ build scripts by generate tools
#
#Email:
#       zuiwuchang@gmail.com
DirRoot=$(cd $(dirname $BASH_SOURCE) && pwd)
if [[ "$OSTYPE" == "msys" ]]; then
	CMake=/f/opt/CMAKE/bin/cmake.exe
else
	CMake=cmake
fi
function check(){
	if [ "$1" != 0 ] ;then
		exit $1
	fi
}
function mkDir(){
	mkdir -p "$1"
	check $?
}

function newFile(){
	echo "$2" > "$1"
	check $?
}
function writeFile(){
	echo "$2" >> "$1"
	check $?
}
function createCppVersion(){
	mkDir "$DirRoot/src"
	filename="$DirRoot/src/version.h"

	tag=`git describe`
	if [ "$tag" == '' ];then
		tag="[unknown tag]"
	fi

	commit=`git rev-parse HEAD`
	if [ "$commit" == '' ];then
		commit="[unknow commit]"
	fi
	
	date=`date +'%Y-%m-%d %H:%M:%S'`

	echo $tag $commit
	echo $date

	newFile $filename	"#ifndef __kc_VERSION_H__"
	writeFile $filename	'#define __kc_VERSION_H__'
	writeFile $filename	''
	writeFile $filename	"#define GIT_TAG \"$tag\""
	writeFile $filename	"#define GIT_COMMIT \"$commit\""
	writeFile $filename	"#define GIT_DATE \"$date\""
	writeFile $filename	''
	writeFile $filename	'#endif // __kc_VERSION_H__'
}
function DisplayHelp(){
	echo "help                 : show help"
	echo "cmake  d/r           : run cmake"
	echo "make   d/r [number]  : run make,number of process default 10"
	echo "clean  d/r           : run make clean"
	echo "remove d/r           : remove cmake tmp files"
	echo "test [v|vf]          : valgrind check memory loss"
}

function runCMake(){
	local dir;
	local mode;
	case $1 in
		r|release)
			dir="$DirRoot/build/release"
			mode="Release"
			echo cmake release
		;;
		*)
			dir="$DirRoot/build/debug"
			mode="Debug"
			echo cmake debug
		;;
	esac
	if [ ! -d "$dir" ];then
		mkdir "$dir" -p
		check $?
	fi

	if [[ "$OSTYPE" == "msys" ]]; then
		echo cmake -G 'MSYS Makefiles' -DCMAKE_BUILD_TYPE=$mode "$DirRoot"
		cd "$dir" && $CMake -G 'MSYS Makefiles' -DCMAKE_BUILD_TYPE=$mode "$DirRoot"
	else
		echo cmake -DCMAKE_BUILD_TYPE=$mode "$DirRoot"
		cd "$dir" && $CMake -DCMAKE_BUILD_TYPE=$mode "$DirRoot"
	fi
}
function runMake(){
	createCppVersion

	local dir;
	local str;
	case $1 in
		r|release)
			dir="$DirRoot/build/release"
			str=release
		;;
		*)
			dir="$DirRoot/build/debug"
			str=debug
		;;
	esac
	# cmake
	if [ ! -d "$dir" ];then
		runCMake $1
	fi

	# make
	echo make $str
	if [[ "$2" == "" ]];then
		echo make -j 10
		cd "$dir" && make -j 10
	else
		echo make -j $2
		cd "$dir" && make -j $2
	fi
}
function runMakeClean(){
	local dir;
	case $1 in
		r|release)
			dir="$DirRoot/build/release"
			echo clean release
		;;
		*)
			dir="$DirRoot/build/debug"
			echo clean debug
		;;
	esac

	echo make clean
	cd "$dir" && make clean
}
function runRemove(){
	local dir;
	case $1 in
		r|release)
			dir="$DirRoot/build/release"
			echo remove release
		;;
		*)
			dir="$DirRoot/build/debug"
			echo remove debug
		;;
	esac
	if [ -d "$dir" ];then
		echo rm $dir -r
		rm "$dir" -r
	fi
}
case $1 in
	cmake)
		runCMake $2
	;;

	make)
		runMake $2 $3
	;;

	clean)
		runMakeClean $2
	;;

	remove)
		runRemove $2
	;;

	test)
		if [[ "$2" == "vf" ]];then
			 valgrind --leak-check=full --show-leak-kinds=all --verbose "$DirRoot/build/debug/binary_c" test
		elif  [[ "$2" == "v" ]];then
			valgrind "$DirRoot/build/debug/binary_c" test
		else
			"$DirRoot/build/debug/binary_c" test
		fi
	;;

	*)
		DisplayHelp
	;;
esac
