#ifndef __kc_MAIN_H__
#define __kc_MAIN_H__
#include <stdlib.h>
void my_free(void *p);
void *my_malloc(size_t n);
void reset_reference();
int check_reference();
#endif // __kc_MAIN_H__