#include <gtest/gtest.h>
#include <kc.h>
int g_reference = 0;
void *my_malloc(size_t n)
{
    ++g_reference;
    return malloc(n);
}
void my_free(void *p)
{
    --g_reference;
    free(p);
}

void reset_reference()
{
    g_reference = 0;
}
int check_reference()
{
    return g_reference;
}
int main(int argc, char *argv[])
{
    kc_set_allocator(my_malloc, my_free);

    testing::InitGoogleTest(&argc, argv);
    int rs = RUN_ALL_TESTS();
    return rs;
}