#include <stdio.h>
#include <stdlib.h>
#include <gtest/gtest.h>

#include <kc.h>
#include "main.h"
#define VAR_CONST_INTS(...) ((int[]){__VA_ARGS__})
#define SLICE_INTS_BOXING(...) VAR_CONST_INTS(__VA_ARGS__), sizeof(VAR_CONST_INTS(__VA_ARGS__)) / sizeof(int)

// 聲明一個 int 類型的 切片
KC_TYPE_SLICE_DECLARE(ints, int)
// 爲 int 切片生成 實現代碼
KC_TYPE_SLICE_IMPLEMENT(ints, int)

#ifndef KC_STRING_DISABLED
TEST(StringTest, HandleNoneZeroInput)
{
    reset_reference();

    const char *source = "cerberus is an idea";
    const size_t length = strlen(source);

    // 定義字符切片
    KC_VAR(kc_string_t, str)
    EXPECT_TRUE(!str.array);
    EXPECT_EQ(str.length, 0);
    {
        // 手動 申請 資源
        EXPECT_TRUE(kc_string_malloc(&str, length, length * 2));
        EXPECT_TRUE(str.array);
        EXPECT_EQ(str.array->length, 2 * length);
        EXPECT_EQ(str.array->reference, 1);
        EXPECT_EQ(str.length, length);
        EXPECT_TRUE(str.data);

        // 設置切片數據
        EXPECT_EQ(kc_string_copy(&str, source, length), length);
        EXPECT_TRUE(!memcmp(str.data, source, str.length));
        EXPECT_TRUE(str.array);
        EXPECT_EQ(str.array->length, 2 * length);
        EXPECT_EQ(str.array->reference, 1);
        EXPECT_EQ(str.length, length);
        EXPECT_TRUE(str.data);
        EXPECT_TRUE(kc_string_equal(&str, source, length));
        EXPECT_TRUE(kc_string_starts_with(&str, KC_C_STRING_BOXING("cer")));
        EXPECT_FALSE(kc_string_starts_with(&str, KC_C_STRING_BOXING("0er")));
        EXPECT_TRUE(kc_string_ends_with(&str, KC_C_STRING_BOXING("idea")));
        EXPECT_FALSE(kc_string_ends_with(&str, KC_C_STRING_BOXING("iaea")));
        EXPECT_EQ(kc_string_first(&str, 0, KC_C_STRING_BOXING(" ")), 8);
        EXPECT_EQ(kc_string_first(&str, 8 + 1, KC_C_STRING_BOXING(" ")), 11);
        EXPECT_EQ(kc_string_first(&str, 11 + 1, KC_C_STRING_BOXING(" ")), 14);
        EXPECT_EQ(kc_string_first(&str, 14 + 1, KC_C_STRING_BOXING("idea")), 15);

        EXPECT_EQ(kc_string_last(&str, 0, KC_C_STRING_BOXING("idea")), 15);
        EXPECT_EQ(kc_string_last(&str, 0, KC_C_STRING_BOXING(" ")), 14);
        EXPECT_EQ(kc_string_last(&str, 4, KC_C_STRING_BOXING(" ")), 14);
        EXPECT_EQ(kc_string_last(&str, 5, KC_C_STRING_BOXING(" ")), 11);
        EXPECT_EQ(kc_string_last(&str, 5, KC_C_STRING_BOXING("cer")), 0);

        // 釋放 切片
        kc_string_detach(&str);
        EXPECT_FALSE(str.array);
        EXPECT_EQ(str.length, 0);
        EXPECT_FALSE(str.data);
    }
    EXPECT_EQ(check_reference(), 0);

    // 設置 數據
    EXPECT_TRUE(kc_string_from(&str, source, length));
    EXPECT_TRUE(str.array);
    EXPECT_EQ(str.array->reference, 1);
    EXPECT_EQ(str.length, length);
    EXPECT_TRUE(str.data);
    EXPECT_TRUE(!memcmp(str.data, source, str.length));
    EXPECT_TRUE(kc_string_equal(&str, source, length));

    {
        const char *c0 = "cerberus";
        const size_t cl0 = strlen(c0);
        // 切取 子切片
        kc_string_t str0 = kc_string_sub(&str, 0, cl0);
        EXPECT_EQ(str.array, str0.array);
        EXPECT_EQ(str.array->reference, 2);
        EXPECT_TRUE(kc_string_equal(&str0, KC_C_STRING_BOXING(c0)));
        EXPECT_TRUE(kc_string_starts_with(&str0, KC_C_STRING_BOXING("cer")));
        EXPECT_FALSE(kc_string_starts_with(&str0, KC_C_STRING_BOXING("ceb")));
        EXPECT_TRUE(kc_string_ends_with(&str0, KC_C_STRING_BOXING("erus")));
        EXPECT_FALSE(kc_string_ends_with(&str0, KC_C_STRING_BOXING("er1s")));
        EXPECT_TRUE(kc_string_starts_with(&str, KC_SLICE_BOXING(&str0)));

        const char *c1 = "is an idea";
        // 切取 子切片
        kc_string_t str1 = kc_string_sub(&str, cl0 + 1, KC_INVALID_SIZE_T);
        EXPECT_EQ(str.array, str1.array);
        EXPECT_EQ(str.array->reference, 3);
        EXPECT_TRUE(kc_string_equal(&str1, KC_C_STRING_BOXING(c1)));
        EXPECT_TRUE(kc_string_starts_with(&str1, KC_C_STRING_BOXING("is a")));
        EXPECT_FALSE(kc_string_starts_with(&str1, KC_C_STRING_BOXING("is b")));
        EXPECT_TRUE(kc_string_ends_with(&str1, KC_C_STRING_BOXING("idea")));
        EXPECT_FALSE(kc_string_ends_with(&str1, KC_C_STRING_BOXING("iaea")));

        // 切取 子切片
        {
            kc_string_t str2 = kc_string_sub(&str1, 3, KC_INVALID_SIZE_T);
            EXPECT_EQ(str.array, str2.array);
            EXPECT_EQ(str.array->reference, 4);
            EXPECT_TRUE(kc_string_equal(&str2, KC_C_STRING_BOXING("an idea")));
            EXPECT_TRUE(kc_string_ends_with(&str1, KC_SLICE_BOXING(&str2)));
            EXPECT_TRUE(kc_string_ends_with(&str2, KC_SLICE_BOXING(&str2)));
            EXPECT_TRUE(kc_string_starts_with(&str2, KC_SLICE_BOXING(&str2)));

            kc_string_detach(&str2); // 釋放子切片
            EXPECT_EQ(str.array->reference, 3);
        }

        kc_string_detach(&str0); // 釋放子切片
        EXPECT_EQ(str.array->reference, 2);

        kc_string_detach(&str1); // 釋放子切片
        EXPECT_EQ(str.array->reference, 1);
    }

    // 釋放 切片
    kc_string_detach(&str);
    EXPECT_FALSE(str.array);
    EXPECT_EQ(str.length, 0);
    EXPECT_FALSE(str.data);
    EXPECT_EQ(check_reference(), 0);

    // append
    EXPECT_TRUE(kc_string_append(&str, KC_C_STRING_BOXING("/home")));
    EXPECT_EQ(str.array->length, 5);
    EXPECT_EQ(str.length, 5);
    EXPECT_TRUE(kc_string_equal(&str, KC_C_STRING_BOXING("/home")));

    EXPECT_TRUE(kc_string_append(&str, KC_C_STRING_BOXING("/kk")));
    EXPECT_EQ(str.array->length, 10);
    EXPECT_EQ(str.length, 5 + 3);
    EXPECT_TRUE(kc_string_equal(&str, KC_C_STRING_BOXING("/home/kk")));

    EXPECT_TRUE(kc_string_append(&str, KC_C_STRING_BOXING("/a")));
    EXPECT_EQ(str.array->length, 10);
    EXPECT_EQ(str.length, 5 + 3 + 2);
    EXPECT_TRUE(kc_string_equal(&str, KC_C_STRING_BOXING("/home/kk/a")));

    // count
    EXPECT_EQ(kc_string_count(&str, KC_C_STRING_BOXING("k")), 2);
    EXPECT_EQ(kc_string_count(&str, KC_C_STRING_BOXING("/a")), 1);
    EXPECT_EQ(kc_string_count(&str, KC_C_STRING_BOXING("/")), 3);

    // split
    kc_string_t strs[4];
    memset(&strs, 0, sizeof(kc_string_t) * 4);
    {
        EXPECT_EQ(kc_string_split(&str, strs, 4, 0, 0), 1);
        EXPECT_EQ(str.array->reference, 1 + 1);
        EXPECT_TRUE(kc_string_equal(&str, KC_SLICE_BOXING(&strs[0])));
        EXPECT_TRUE(kc_string_equal(&str, KC_C_STRING_BOXING("/home/kk/a")));

        EXPECT_EQ(kc_string_split(&str, strs, 1, KC_C_STRING_BOXING("/")), 1);
        EXPECT_EQ(str.array->reference, 1 + 1);
        EXPECT_TRUE(kc_string_equal(&str, KC_SLICE_BOXING(&strs[0])));
        EXPECT_TRUE(kc_string_equal(&str, KC_C_STRING_BOXING("/home/kk/a")));
    }

    kc_string_detach(&strs[0]);
    EXPECT_EQ(str.array->reference, 1);
    {
        EXPECT_EQ(kc_string_split(&str, strs, 4, KC_C_STRING_BOXING("/")), 4);
        EXPECT_TRUE(kc_string_equal(&strs[0], KC_C_STRING_BOXING("")));
        EXPECT_TRUE(kc_string_equal(&strs[1], KC_C_STRING_BOXING("home")));
        EXPECT_TRUE(kc_string_equal(&strs[2], KC_C_STRING_BOXING("kk")));
        EXPECT_TRUE(kc_string_equal(&strs[3], KC_C_STRING_BOXING("a")));
        EXPECT_EQ(str.array->reference, (1 + 4));
        for (size_t i = 0; i < 4; i++)
        {
            kc_string_detach(&strs[i]);
            EXPECT_FALSE(strs[i].array);
            EXPECT_EQ(strs[i].length, 0);
            EXPECT_FALSE(strs[i].data);
        }

        EXPECT_EQ(kc_string_split(&str, strs, 3, KC_C_STRING_BOXING("/")), 3);
        EXPECT_TRUE(kc_string_equal(&strs[0], KC_C_STRING_BOXING("")));
        EXPECT_TRUE(kc_string_equal(&strs[1], KC_C_STRING_BOXING("home")));
        EXPECT_TRUE(kc_string_equal(&strs[2], KC_C_STRING_BOXING("kk/a")));
        EXPECT_EQ(str.array->reference, (1 + 3));
        for (size_t i = 0; i < 3; i++)
        {
            kc_string_detach(&strs[i]);
            EXPECT_FALSE(strs[i].array);
            EXPECT_EQ(strs[i].length, 0);
            EXPECT_FALSE(strs[i].data);
        }

        EXPECT_EQ(kc_string_split(&str, strs, 4, KC_C_STRING_BOXING("/a")), 2);
        EXPECT_TRUE(kc_string_equal(&strs[0], KC_C_STRING_BOXING("/home/kk")));
        EXPECT_TRUE(kc_string_equal(&strs[1], KC_C_STRING_BOXING("")));
        EXPECT_EQ(str.array->reference, (1 + 2));
        for (size_t i = 0; i < 2; i++)
        {
            kc_string_detach(&strs[i]);
            EXPECT_FALSE(strs[i].array);
            EXPECT_EQ(strs[i].length, 0);
            EXPECT_FALSE(strs[i].data);
        }
    }
    EXPECT_EQ(str.array->reference, 1);
    kc_string_detach(&str);

    EXPECT_EQ(check_reference(), 0);
}
#endif // KC_STRING_DISABLED

TEST(IntSliceTest, HandleNoneZeroInput)
{
    reset_reference();

    const int source[] = {1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5};
    const size_t length = 15;

    // 定義 int 切片
    KC_VAR(kc_ints_t, str)
    EXPECT_TRUE(!str.array);
    EXPECT_EQ(str.length, 0);
    {
        // 手動 申請 資源
        EXPECT_TRUE(kc_ints_malloc(&str, length, length * 2));
        EXPECT_TRUE(str.array);
        EXPECT_EQ(str.array->length, 2 * length);
        EXPECT_EQ(str.array->reference, 1);
        EXPECT_EQ(str.length, length);
        EXPECT_TRUE(str.data);

        // 設置切片數據
        EXPECT_EQ(kc_ints_copy(&str, source, length), length);
        EXPECT_TRUE(!memcmp(str.data, source, str.length));
        EXPECT_TRUE(str.array);
        EXPECT_EQ(str.array->length, 2 * length);
        EXPECT_EQ(str.array->reference, 1);
        EXPECT_EQ(str.length, length);
        EXPECT_TRUE(str.data);
        EXPECT_TRUE(kc_ints_equal(&str, source, length));
        EXPECT_TRUE(kc_ints_starts_with(&str, SLICE_INTS_BOXING(1, 2, 3)));
        EXPECT_FALSE(kc_ints_starts_with(&str, SLICE_INTS_BOXING(0, 2, 3)));
        EXPECT_TRUE(kc_ints_ends_with(&str, SLICE_INTS_BOXING(3, 4, 5)));
        EXPECT_FALSE(kc_ints_ends_with(&str, SLICE_INTS_BOXING(3, 3, 5)));
        EXPECT_EQ(kc_ints_first(&str, 0, SLICE_INTS_BOXING(2)), 1);
        EXPECT_EQ(kc_ints_first(&str, 1 + 1, SLICE_INTS_BOXING(2)), 1 + 5);
        EXPECT_EQ(kc_ints_first(&str, 1 + 5 + 1, SLICE_INTS_BOXING(2)), 1 + 5 + 5);
        EXPECT_EQ(kc_ints_first(&str, 1 + 5 + 1, SLICE_INTS_BOXING(2, 3, 4, 5)), 1 + 5 + 5);

        EXPECT_EQ(kc_ints_last(&str, 0, SLICE_INTS_BOXING(2, 3, 4, 5)), 1 + 5 + 5);
        EXPECT_EQ(kc_ints_last(&str, 0, SLICE_INTS_BOXING(4)), 13);
        EXPECT_EQ(kc_ints_last(&str, 2, SLICE_INTS_BOXING(4)), 13 - 5);
        EXPECT_EQ(kc_ints_last(&str, 2 + 5, SLICE_INTS_BOXING(4)), 13 - 5 - 5);
        EXPECT_EQ(kc_ints_last(&str, 3 + 5 + 5, SLICE_INTS_BOXING(1, 2)), 0);

        // 釋放 切片
        kc_ints_detach(&str);
        EXPECT_FALSE(str.array);
        EXPECT_EQ(str.length, 0);
        EXPECT_FALSE(str.data);
    }
    EXPECT_EQ(check_reference(), 0);

    // 設置 數據
    EXPECT_TRUE(kc_ints_from(&str, source, length));
    EXPECT_TRUE(str.array);
    EXPECT_EQ(str.array->reference, 1);
    EXPECT_EQ(str.length, length);
    EXPECT_TRUE(str.data);
    EXPECT_TRUE(!memcmp(str.data, source, str.length));
    EXPECT_TRUE(kc_ints_equal(&str, source, length));

    {
        const int c0[] = {1, 2, 3, 4, 5};
        const size_t cl0 = 5;
        // 切取 子切片
        kc_ints_t str0 = kc_ints_sub(&str, 0, cl0);
        EXPECT_EQ(str.array, str0.array);
        EXPECT_EQ(str.array->reference, 2);
        EXPECT_TRUE(kc_ints_equal(&str0, c0, cl0));
        EXPECT_TRUE(kc_ints_starts_with(&str0, SLICE_INTS_BOXING(1, 2, 3)));
        EXPECT_FALSE(kc_ints_starts_with(&str0, SLICE_INTS_BOXING(1, 2, 4)));
        EXPECT_TRUE(kc_ints_ends_with(&str0, SLICE_INTS_BOXING(3, 4, 5)));
        EXPECT_FALSE(kc_ints_ends_with(&str0, SLICE_INTS_BOXING(3, 4, 1, 5)));
        EXPECT_TRUE(kc_ints_starts_with(&str, KC_SLICE_BOXING(&str0)));

        const int c1[] = {2, 3, 4, 5, 1, 2, 3, 4, 5};
        // 切取 子切片
        kc_ints_t str1 = kc_ints_sub(&str, cl0 + 1, KC_INVALID_SIZE_T);
        EXPECT_EQ(str.array, str1.array);
        EXPECT_EQ(str.array->reference, 3);
        EXPECT_TRUE(kc_ints_equal(&str1, c1, 9));
        EXPECT_TRUE(kc_ints_starts_with(&str1, SLICE_INTS_BOXING(2, 3, 4)));
        EXPECT_FALSE(kc_ints_starts_with(&str1, SLICE_INTS_BOXING(2, 3, 5)));
        EXPECT_TRUE(kc_ints_ends_with(&str1, SLICE_INTS_BOXING(5, 1, 2, 3, 4, 5)));
        EXPECT_FALSE(kc_ints_ends_with(&str1, SLICE_INTS_BOXING(5, 1, 2, 4, 4, 5)));

        // 切取 子切片
        {
            kc_ints_t str2 = kc_ints_sub(&str1, 4, KC_INVALID_SIZE_T);
            EXPECT_EQ(str.array, str2.array);
            EXPECT_EQ(str.array->reference, 4);
            EXPECT_TRUE(kc_ints_equal(&str2, SLICE_INTS_BOXING(1, 2, 3, 4, 5)));
            EXPECT_TRUE(kc_ints_ends_with(&str1, KC_SLICE_BOXING(&str2)));
            EXPECT_TRUE(kc_ints_ends_with(&str2, KC_SLICE_BOXING(&str2)));
            EXPECT_TRUE(kc_ints_starts_with(&str2, KC_SLICE_BOXING(&str2)));

            kc_ints_detach(&str2); // 釋放子切片
            EXPECT_EQ(str.array->reference, 3);
        }

        kc_ints_detach(&str0); // 釋放子切片
        EXPECT_EQ(str.array->reference, 2);

        kc_ints_detach(&str1); // 釋放子切片
        EXPECT_EQ(str.array->reference, 1);
    }

    // 釋放 切片
    kc_ints_detach(&str);
    EXPECT_FALSE(str.array);
    EXPECT_EQ(str.length, 0);
    EXPECT_FALSE(str.data);
    EXPECT_EQ(check_reference(), 0);

    // append
    EXPECT_TRUE(kc_ints_append(&str, SLICE_INTS_BOXING(0, 1, 2, 3, 4)));
    EXPECT_EQ(str.array->length, 5);
    EXPECT_EQ(str.length, 5);
    EXPECT_TRUE(kc_ints_equal(&str, SLICE_INTS_BOXING(0, 1, 2, 3, 4)));

    EXPECT_TRUE(kc_ints_append(&str, SLICE_INTS_BOXING(0, 5, 5)));
    EXPECT_EQ(str.array->length, 10);
    EXPECT_EQ(str.length, 5 + 3);
    EXPECT_TRUE(kc_ints_equal(&str, SLICE_INTS_BOXING(0, 1, 2, 3, 4, 0, 5, 5)));

    EXPECT_TRUE(kc_ints_append(&str, SLICE_INTS_BOXING(0, 6)));
    EXPECT_EQ(str.array->length, 10);
    EXPECT_EQ(str.length, 5 + 3 + 2);
    EXPECT_TRUE(kc_ints_equal(&str, SLICE_INTS_BOXING(0, 1, 2, 3, 4, 0, 5, 5, 0, 6)));

    // count
    EXPECT_EQ(kc_ints_count(&str, SLICE_INTS_BOXING(5)), 2);
    EXPECT_EQ(kc_ints_count(&str, SLICE_INTS_BOXING(0, 6)), 1);
    EXPECT_EQ(kc_ints_count(&str, SLICE_INTS_BOXING(0)), 3);

    // split
    kc_ints_t strs[4];
    memset(&strs, 0, sizeof(kc_ints_t) * 4);
    {
        EXPECT_EQ(kc_ints_split(&str, strs, 4, 0, 0), 1);
        EXPECT_EQ(str.array->reference, 1 + 1);
        EXPECT_TRUE(kc_ints_equal(&str, KC_SLICE_BOXING(&strs[0])));
        EXPECT_TRUE(kc_ints_equal(&str, SLICE_INTS_BOXING(0, 1, 2, 3, 4, 0, 5, 5, 0, 6)));

        EXPECT_EQ(kc_ints_split(&str, strs, 1, SLICE_INTS_BOXING(0)), 1);
        EXPECT_EQ(str.array->reference, 1 + 1);
        EXPECT_TRUE(kc_ints_equal(&str, KC_SLICE_BOXING(&strs[0])));
        EXPECT_TRUE(kc_ints_equal(&str, SLICE_INTS_BOXING(0, 1, 2, 3, 4, 0, 5, 5, 0, 6)));
    }

    kc_ints_detach(&strs[0]);
    EXPECT_EQ(str.array->reference, 1);
    {
        EXPECT_EQ(kc_ints_split(&str, strs, 4, SLICE_INTS_BOXING(0)), 4);
        EXPECT_TRUE(kc_ints_equal(&strs[0], SLICE_INTS_BOXING()));
        EXPECT_TRUE(kc_ints_equal(&strs[1], SLICE_INTS_BOXING(1, 2, 3, 4)));
        EXPECT_TRUE(kc_ints_equal(&strs[2], SLICE_INTS_BOXING(5, 5)));
        EXPECT_TRUE(kc_ints_equal(&strs[3], SLICE_INTS_BOXING(6)));
        EXPECT_EQ(str.array->reference, (1 + 4));
        for (size_t i = 0; i < 4; i++)
        {
            kc_ints_detach(&strs[i]);
            EXPECT_FALSE(strs[i].array);
            EXPECT_EQ(strs[i].length, 0);
            EXPECT_FALSE(strs[i].data);
        }

        EXPECT_EQ(kc_ints_split(&str, strs, 3, SLICE_INTS_BOXING(0)), 3);
        EXPECT_TRUE(kc_ints_equal(&strs[0], SLICE_INTS_BOXING()));
        EXPECT_TRUE(kc_ints_equal(&strs[1], SLICE_INTS_BOXING(1, 2, 3, 4)));
        EXPECT_TRUE(kc_ints_equal(&strs[2], SLICE_INTS_BOXING(5, 5, 0, 6)));
        EXPECT_EQ(str.array->reference, (1 + 3));
        for (size_t i = 0; i < 3; i++)
        {
            kc_ints_detach(&strs[i]);
            EXPECT_FALSE(strs[i].array);
            EXPECT_EQ(strs[i].length, 0);
            EXPECT_FALSE(strs[i].data);
        }

        EXPECT_EQ(kc_ints_split(&str, strs, 4, SLICE_INTS_BOXING(0, 6)), 2);
        EXPECT_TRUE(kc_ints_equal(&strs[0], SLICE_INTS_BOXING(0, 1, 2, 3, 4, 0, 5, 5)));
        EXPECT_TRUE(kc_ints_equal(&strs[1], SLICE_INTS_BOXING()));
        EXPECT_EQ(str.array->reference, (1 + 2));
        for (size_t i = 0; i < 2; i++)
        {
            kc_ints_detach(&strs[i]);
            EXPECT_FALSE(strs[i].array);
            EXPECT_EQ(strs[i].length, 0);
            EXPECT_FALSE(strs[i].data);
        }
    }
    EXPECT_EQ(str.array->reference, 1);
    kc_ints_detach(&str);

    EXPECT_EQ(check_reference(), 0);
}