#include <stdio.h>
#include <gtest/gtest.h>

#include <kc.h>
#include "main.h"
extern int g_reference;

TEST(PointerTest, HandleNoneZeroInput)
{
    reset_reference();

    KC_VAR(kc_pointer_t, p)
    EXPECT_TRUE(!p.pointer);
    EXPECT_EQ(p.reference, 0);

    EXPECT_TRUE(kc_pointer_init(&p, 4));
    EXPECT_TRUE(p.pointer);
    EXPECT_EQ(p.reference, 1);
    EXPECT_EQ(g_reference, 1);

    EXPECT_TRUE(kc_pointer_init(&p, 8));
    EXPECT_TRUE(p.pointer);
    EXPECT_EQ(p.reference, 1);
    EXPECT_EQ(g_reference, 1);

    EXPECT_EQ(1, kc_pointer_increment(&p));
    EXPECT_TRUE(p.pointer);
    EXPECT_EQ(p.reference, 2);
    EXPECT_EQ(g_reference, 1);

    EXPECT_EQ(2, kc_pointer_decrement(&p));
    EXPECT_TRUE(p.pointer);
    EXPECT_EQ(p.reference, 1);
    EXPECT_EQ(g_reference, 1);

    EXPECT_EQ(1, kc_pointer_decrement(&p));
    EXPECT_TRUE(!p.pointer);
    EXPECT_EQ(p.reference, 0);
    EXPECT_EQ(g_reference, 0);

    EXPECT_EQ(0, kc_pointer_decrement(&p));
    EXPECT_TRUE(!p.pointer);
    EXPECT_EQ(p.reference, 0);
    EXPECT_EQ(g_reference, 0);
}
TEST(PointerArrayTest, HandleNoneZeroInput)
{
    reset_reference();

    KC_VAR(kc_array_t, p)
    EXPECT_TRUE(!p.pointer);
    EXPECT_EQ(p.reference, 0);

    EXPECT_TRUE(kc_array_init(&p, 4, 10));
    EXPECT_TRUE(p.pointer);
    EXPECT_EQ(p.length, 10);
    EXPECT_EQ(p.reference, 1);
    EXPECT_EQ(g_reference, 1);

    EXPECT_TRUE(kc_array_init(&p, 8, 5));
    EXPECT_TRUE(p.pointer);
    EXPECT_EQ(p.length, 5);
    EXPECT_EQ(p.reference, 1);
    EXPECT_EQ(g_reference, 1);

    EXPECT_EQ(1, kc_array_increment(&p));
    EXPECT_TRUE(p.pointer);
    EXPECT_EQ(p.length, 5);
    EXPECT_EQ(p.reference, 2);
    EXPECT_EQ(g_reference, 1);

    EXPECT_EQ(2, kc_array_decrement(&p));
    EXPECT_TRUE(p.pointer);
    EXPECT_EQ(p.length, 5);
    EXPECT_EQ(p.reference, 1);
    EXPECT_EQ(g_reference, 1);

    EXPECT_EQ(1, kc_array_decrement(&p));
    EXPECT_TRUE(!p.pointer);
    EXPECT_EQ(p.length, 0);
    EXPECT_EQ(p.reference, 0);
    EXPECT_EQ(g_reference, 0);

    EXPECT_EQ(0, kc_array_decrement(&p));
    EXPECT_TRUE(!p.pointer);
    EXPECT_EQ(p.reference, 0);
    EXPECT_EQ(g_reference, 0);
}