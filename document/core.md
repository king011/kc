# core

```
#include <core.h>
```

core 模塊提供了 一些最基礎的核心功能 和一些幾乎被所有編程需要使用到的宏

通常其它模塊都要依賴此實現

# Macro

BOOL TRUE FALSE 被定義來模擬 布爾類型

KC_VAR 宏用來定義一個變量 並使用 memset 對齊執行置0操作

KC_INVALID_SIZE_T 被定義爲 ((size_t)-1) 用來表示一個無效的索引


# allocator

kc_set_allocator 函數用來設置 一個 內存分配器 所有 kc 模塊都使用此分配器 分配內存 如果沒有設置過則使用 默認的 malloc 和 free 操作內存

```
typedef void *(*kc_malloc_ft)(size_t);
typedef void (*kc_free_ft)(void *);

// 設置內存分配器 所有 kc 模塊將 使用 此分配器 進行內存分配
void kc_set_allocator(kc_malloc_ft m, kc_free_ft f);

// 分塊內存
void *kc_malloc(size_t n);

// 釋放內存
void kc_free(void *p);
```

kc_malloc 和 kc_free 會檢查是否設置了 allocator 如果設置了使用 allocator 操作內存 否則 使用 malloc 和 free 操作內存

# kc_pointer_t

kc_pointer_t 是一個 帶引用計數的 指針 定義如下

```
typedef struct
{
    void *pointer;
    atomic_size_t reference;
} kc_pointer_t;
```

reference 使用 原子變量保證多線程下 可以正確的釋放資源 

> 原子變量只能保證多線程下 reference 的值正確 如果多線程下 pointer 和 reference 被同時 設置 需要調用者 自行同步

kc_pointer_t 提供了一些方便操作的成員函數 

```
// 如果 pointer 有值 則 使用 reference 自增1 並返回原值
size_t kc_pointer_increment(kc_pointer_t *self);

// 如果 pointer 有值 則 使用 reference 自減1 並返回原值 並且當 reference 由非0變爲0時 自動調用 kc_free 釋放內存
size_t kc_pointer_decrement(kc_pointer_t *self);

// 爲 kc_pointer_t 申請 大小爲 size 的 內存 如果成功 返回 TRUE 並釋放 kc_pointer_t 之前佔用的內存
BOOL kc_pointer_init(kc_pointer_t *self, size_t size);

// 釋放 kc_pointer_t 之前佔用的內存 並且 設置當前佔用內存爲 pointer
BOOL kc_pointer_set(kc_pointer_t *self, void *pointer);

// 打印 測試信息到 標準輸出
void kc_pointer_stdout(kc_pointer_t *self, const char *prefix);
```
# kc_array_t

kc_array_t 類似 kc_pointer_t 但 通常用於 保存 數組指針

```
typedef struct
{
    void *pointer;
    size_t length;
    atomic_size_t reference;
} kc_array_t;
```

相比 kc_pointer_t 而言 kc_array_t 只是簡單的增加了一個 length 來表示數組大小

> length 表示的是數組大小 而非 數組佔用內存大小 比如 int 數組佔用內存大小應該爲 sizeof(int)*length

kc_array_t 的成員函數 也和 kc_pointer_t 類似

```
// 如果 pointer 有值 則 使用 reference 自增1 並返回原值
size_t kc_array_increment(kc_array_t *self);

// 如果 pointer 有值 則 使用 reference 自減1 並返回原值 並且當 reference 由非0變爲0時 自動調用 kc_free 釋放內存
size_t kc_array_decrement(kc_array_t *self);

// 爲 kc_array_t 申請 大小爲 size*length 的 內存 如果成功 返回 TRUE 並釋放 kc_array_t 之前佔用的內存 同時設置數組大小爲 length
BOOL kc_array_init(kc_array_t *self, const size_t size, const size_t length);

// 釋放 kc_array_t 之前佔用的內存 並且 設置當前佔用內存爲 pointer 當前數據大小爲 length
BOOL kc_array_set(kc_array_t *self, void *pointer, const size_t length);

// 打印 測試信息到 標準輸出
void kc_pointer_stdout(kc_pointer_t *self, const char *prefix);
```