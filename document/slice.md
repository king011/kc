# slice

```
#include <slice.h>
```

dependents : core

* 切片是從數組上 切取的一個小塊 保留了到原數組的引用 使用切片可以更高效的利用 內存
* 切片是從數組上 切取的一個小塊 並且支持從當前切片上 切取更小的塊
* 所有切取操作 都不會重寫分配內存 只是創建了一個到 原數組的引用
* 切片支持自動擴容 當原數組容量足夠時 會直接使用原數組 當原數組容量不夠時 將創建一個 新的數組 並關聯新數組

# KC_TYPE_SLICE_DECLARE KC_TYPE_SLICE_IMPLEMENT 代碼生成器

由於 c 不支持模板 無法爲所有類別包括自定義類別 實現通用的 切片代碼 故 kc庫定義了 兩個 宏 KC_TYPE_SLICE_DECLARE 和 KC_TYPE_SLICE_IMPLEMENT 來作爲代碼生成器

使用 這個兩個宏 可以 輕易的 爲 自定義 或 kc 庫未提供的 類別 提供 slice 實現

KC_TYPE_SLICE_DECLARE 和 KC_TYPE_SLICE_IMPLEMENT 宏 都需要傳入一個 切片名稱 以及 切片數據類型 即會自動生成一個新類型 並實現切片操作

比如 要實現一個 int 型別的 切片 kc_ints_t

1. 在一個 .h 文件中 添加 `KC_TYPE_SLICE_DECLARE(ints,int)`
2. 在一個 .c 文件中 添加 `KC_TYPE_SLICE_IMPLEMENT(ints,int)`

上述兩個步驟 會創建 kc_ints_t 所需要的全部 聲明與實現 代碼

# 切片函數

所有切片函數都是由 KC_TYPE_SLICE_DECLARE 和 KC_TYPE_SLICE_IMPLEMENT 宏創建 除了名稱不同外 用法都是一致的

切片提供的函數 可參照後文的 kc_string_t

# kc_string_t

kc_string_t 是 kc 庫默認 提供的 字符串 定義如下

```
KC_TYPE_SLICE_DECLARE(string, char)
void kc_string_stdout_ex(kc_string_t *self, const char *prefix);
```

```
KC_TYPE_SLICE_IMPLEMENT(string, char)
void kc_string_stdout_ex(kc_string_t *self, const char *prefix)
{
    ...
}
```

除了 kc_string_stdout_ex 函數外 kc_string_t 和 KC_TYPE_SLICE_DECLARE 聲明的其它切片 沒有任何區別

kc_string_t 提供了如下成員函數
```
// 使 dst 切片 附加到 self 切片所關聯的數組 並使 dst 和 self 保存一致
void kc_string_attach(kc_string_t * self, kc_string_t * dst);

// 斷開 self 和 關聯數組 的聯繫 此後 如果關聯數組不再被任何東西使用 則自動釋放關聯數組
void kc_string_detach(kc_string_t * self);

// 由 c 數組 創建 切片
BOOL kc_string_from(kc_string_t * self, const char *src, size_t length);

// 創建一個大小爲length的切片 並爲其創建一個新的大小爲capacity的關聯數組 
BOOL kc_string_malloc(kc_string_t * self, size_t length, size_t capacity);

// 從self切片切取一個 [begin,end) 的小塊作爲 新切片 返回
kc_string_t kc_string_sub(kc_string_t * self, size_t begin, size_t end);

// 將 c 數組 拷貝 min(切片大小,length)個元素到 切片 並返回拷貝元素數量
size_t kc_string_t kc_string_copy(kc_string_t * self, const char *src, size_t length);

// 返回 切片保存內容 是否和 c數組 保存內容一致 memcmp
BOOL kc_string_equal(kc_string_t * self, const char *src, size_t length);

// 將 切片 全部 填充爲 指定值
void kc_string_fill_value(kc_string_t * self, char value);
// 將 切片 全部 填充爲 0
void kc_string_fill(kc_string_t * self);

// 返回 切片 是否以 指定 c 數組 開始
BOOL kc_string_starts_with(kc_string_t * self, const type *tag, size_t length);
// 返回 切片 是否以 指定 c 數組 結束
BOOL kc_string_ends_with(kc_string_t * self, const type *tag, size_t length);
// 返回 切片中 第一次出現 指定 c 數組的 索引 或則 KC_INVALID_SIZE_T 
size_t kc_string_first(kc_string_t * self, size_t skip, const type *tag, size_t length);
// 返回 切片中 最後一次出現 指定 c 數組的 索引 或則 KC_INVALID_SIZE_T 
size_t kc_string_last(kc_string_t * self, size_t skip, const type *tag, size_t length);

// 將指定 c數組 擴充到 切片尾 如果 切片關聯的數據容量不夠 將自動 擴容
// 如果 append 失敗 保證 原 切片 不會有任何變化
BOOL kc_string_append(kc_string_t * self, const type *values, size_t length);

// 統計 切片中 包含 多少個 指定的 c 數組
size_t kc_string_count(kc_string_t * self, const type *sub, size_t length);

// 將切片self 使用指定c數組進行分割 至多會分割 n個元素 並 輸出到 output 最後返回 分割了多少個 子切片
size_t kc_string_split(kc_string_t * self, kc_string_t * output, size_t n, const type *sep, size_t length);

// 打印 測試信息到 標準輸出
BOOL kc_string_stdout(kc_string_t *self, const char *prefix);

// 只有字符串 提供了 此函數 類似 kc_string_stdout 不同同時會打印出 切片對於的 c 字符串
BOOL kc_string_stdout_ex(kc_string_t *self, const char *prefix);
```

# Macro

很多切片操作都需要傳入 c 數組 KC_SLICE_BOXING 宏 提供了一個 簡便的 寫法 其定義如下

```
#define KC_SLICE_BOXING(self) ((self)->data), ((self)->length)
```

對於 c 字符串 提供了 KC_C_STRING_BOXING 宏

```
#define KC_C_STRING_BOXING(str) (str), strlen((str))
```